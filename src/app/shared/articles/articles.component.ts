import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  articles: any;
  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.getArticles()
    .subscribe(
      (response) => this.articles = response );
  }
}
