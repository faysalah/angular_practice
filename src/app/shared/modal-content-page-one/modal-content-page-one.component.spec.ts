import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalContentPageOneComponent } from './modal-content-page-one.component';

describe('ModalContentPageOneComponent', () => {
  let component: ModalContentPageOneComponent;
  let fixture: ComponentFixture<ModalContentPageOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalContentPageOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContentPageOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
