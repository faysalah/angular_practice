import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class SharedService {
  baseurl = 'http://localhost:57594/';
  constructor(private http: Http) { }
  getArticles() {
    return this.http.get(this.baseurl + 'Author/Articles')
    .map((response: Response) => {
      return response.json();
    });
  }

  getArticleById() {

  }

  getManager() {

  }

  changePassword() {

  }
}
