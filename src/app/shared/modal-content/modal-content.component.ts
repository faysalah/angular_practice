import { Component, Inject,  OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ModalContentPageOneComponent } from "../modal-content-page-one/modal-content-page-one.component";
@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.css']
})
export class ModalContentComponent implements OnInit {

  ngOnInit() {
  }
  constructor(
    public dialogRef: MatDialogRef<ModalContentComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ModalContentPageOneComponent, {
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
