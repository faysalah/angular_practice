import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalContentPageTwoComponent } from './modal-content-page-two.component';

describe('ModalContentPageTwoComponent', () => {
  let component: ModalContentPageTwoComponent;
  let fixture: ComponentFixture<ModalContentPageTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalContentPageTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContentPageTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
