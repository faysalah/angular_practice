import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  filesToUpload: Array<File> = [];
  constructor(private http: Http) { }

  ngOnInit() {
  }
  // onSubmit(form) {
  //   const files: Array<File> = this.filesToUpload;
  //   const formData: any = new FormData();
  //   // formData.append('string', form.value.fineName, 'fileName');
  //   formData.append('uploads[]', files[0], files[0]['name']);
  //   console.log(formData);
  //   console.log(form.value);
  // }

onSubmit(form) {
  console.log(form.value);
  this.fileUpload();
}

fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
}

fileUpload() {
  const formData: any = new FormData();
  const files: Array<File> = this.filesToUpload;

  for (let i = 0; i < files.length; i++) {
       formData.append('uploads[]', files[i], files[i]['name']);
  }

 this.http.post('http://localhost:57594/api/FileUpload', formData)
 .subscribe(
   () => { alert('sucessful.'); },
   () => { alert('failed.'); }
 );
}
}
