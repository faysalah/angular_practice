import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegistrationComponent } from './authentication/registration/registration.component';
import { AuthGuard } from './authentication/auth.guard';
import { AuthenticationService } from './authentication/authentication.service';
import { WarningComponent } from './authentication/warning/warning.component';
import { ManagerComponent } from './shared/manager/manager.component';
import { ArticleComponent } from './shared/article/article.component';
import { ArticlesComponent } from './shared/articles/articles.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { SharedService } from './shared/shared.service';
import { InfoEditComponent } from './shared/info-edit/info-edit.component';
import { InfoViewComponent } from './shared/info-view/info-view.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
// import { HoverdirectiveDirective } from './shared/hoverdirective.directive';
import { ModalComponent } from './shared/modal/modal.component';
import { ModalContentComponent } from './shared/modal-content/modal-content.component';
import { ModalContentPageOneComponent } from './shared/modal-content-page-one/modal-content-page-one.component';
import { ModalContentPageTwoComponent } from './shared/modal-content-page-two/modal-content-page-two.component';


const routing = RouterModule.forRoot([
  { path: '', redirectTo: '/articles', pathMatch: 'full' },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'article', component: ArticleComponent },
  { path: 'file-upload', component: FileUploadComponent },
  { path: 'modal-page', component: ModalComponent },

  // { path: 'manager', component: ManagerComponent, canActivate: [AuthGuard] , children : [
  {
    path: 'manager', component: ManagerComponent, children: [
      { path: '', redirectTo: 'infoview', pathMatch: 'full' },
      { path: 'infoedit', component: InfoEditComponent },
      { path: 'infoview', component: InfoViewComponent },
    ]
  },
  { path: 'warning', component: WarningComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: '**', redirectTo: 'not-found' }
]);


import {
  MatButtonModule,
  MatCheckboxModule,
  MatTabsModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatDialogModule
} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegistrationComponent,
    ArticlesComponent,
    WarningComponent,
    ArticleComponent,
    ManagerComponent,
    NotFoundComponent,
    InfoEditComponent,
    InfoViewComponent,
    FileUploadComponent,
    ModalComponent,
    ModalContentComponent,
    ModalContentPageOneComponent,
    ModalContentPageTwoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    FormsModule,
    HttpModule,
    MatButtonModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule
  ],
  providers: [AuthGuard, AuthenticationService, SharedService, ArticleComponent],
  bootstrap: [AppComponent],
  entryComponents: [ModalContentComponent]
})
export class AppModule { }
