import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }
  onSubmit(form) {
    this.authenticationService.registration(form)
    .subscribe(
      response => {
        confirm('signup successful.');
        this.router.navigate(['/login']);
      },
      error => {
        alert('signup failed.');
    });
  }
}
