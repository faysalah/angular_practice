import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authentication: AuthenticationService, private router: Router) { }
  ngOnInit() {
  }

  onSubmit(formvalue) {
    this.authentication.login(formvalue)
    .subscribe(
      response => {
        confirm('Login successful.');
        this.router.navigate(['/articles']);
      },
      error => {
        alert('Login failed');
    });
  }
}
