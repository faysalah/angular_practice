import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }
  baseurl = 'http://localhost:57594/';
  registration(data: any) {
    const body = JSON.stringify({ email: data.email, password: data.password, confirmPassword: data.confirmPassword });
    const headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.baseurl + 'api/Account/Register', body, { headers: headers });
  }

  login(data: any) {
    const body = 'username=' + data.email + '&password=' + data.password + '&grant_type=password';
    return this.http.post(this.baseurl + 'Token', body)
      .map((response: Response) => {
        return response.json();
      }
      ).do(
      response => {
        sessionStorage.setItem('accessToken', 'bearer ' + response.access_token);
        sessionStorage.setItem('userId', response.userName);
      }
      );
  }

  logout() {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('userId');
    return true;
  }

  getName() {
    return sessionStorage.getItem('userId');
  }

  checkToken() {
    return !!sessionStorage.getItem('accessToken');
  }
}
